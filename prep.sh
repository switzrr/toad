#!/bin/bash
# on arch: install extra/python-black, extra/python-isort and aur/jfmt :3
# please run before committing code <3 -switz

echo "Sorting imports with isort"
python -m isort .
echo "Formatting toad.py with black"
python3 -m black toad.py
echo "Formatting jsons with jfmt"
for file in ./*.json; do
    echo "Formatting $file with jfmt"
    jfmt -i "$file"
done
