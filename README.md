# toad

minimalistic cli based todo list tracker

## Committing
Requirements (package name):
- `black` (`extra/python-black`)
- `isort` (`extra/python-isort`)
- `jfmt` (`aur/jfmt`)

Before committing code, run `prep.sh` or `make format` which runs `prep.sh` anyways.