#!python3
# -*- coding: utf-8 -*-

# im on arch so using a normal python3 shebang doesn't work
# since everything is in a venv. sorry :(

# region GPLv3 notice
# toad: minimalistic cli based todo list tracker
# Copyright (C) 2024 switz (switzrr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# endregion GPLv3 notice

# region INFO
# ------------------------------------------------------------------------INFO-
# Author: Developed by bioluminescent (gpg -- use other altkey)
#  Alternatively, there's at least 10 other aliases that could go here.
# Name: toad
# Description: minimalistic cli (soon to be tui) based todo list tracker.
# Version: v0.0.3a
# Version Date: 29.03.24
# Date: 25.03.24-29.03.24
# License: GPL-3.0-only
#  Summary: https://choosealicense.com/licenses/gpl-3.0/
# -----------------------------------------------------------------------------
# endregion INFO

import json
import os
import typing

if os.path.exists("settings.json"):
    settings = open("settings.json")
    Config = json.loads(settings.read())
    settings.close()
    # manual garbage collection just in case. :)
    del settings
else:
    Config = {
        "save_file": "save.json",
        "status_box": {"complete": "[x]", "incomplete": "[ ]"},
        "status": {
            "complete": [
                True,
                1,
                "1",
                "True",
                "true",
                "x",
                "[x]",
                "complete",
                "completed",
            ],
            "incomplete": [
                False,
                0,
                "0",
                "False",
                "false",
                " ",
                "[ ]",
                "incomplete",
                "uncompleted",
            ],
        },
    }

Config["status"]["complete"] += Config["status_box"]["complete"]
Config["status"]["incomplete"] += Config["status_box"]["incomplete"]


class Commands:
    @staticmethod
    def parse(what: str):
        try:
            command, argument = what.split(maxsplit=1)
        except ValueError:
            command = what
            argument = None
        match command.lower():
            case "":
                pass
            case "exit!" | "!exit" | ":q!":
                Commands.exit(save=False)
            case "exit" | ":q":
                Commands.exit(save=True, location=argument)
            case "gpl":
                Commands.gpl(section=argument)
            case "help":
                Commands.help(command=argument)
            case "load":
                Commands.load(location=argument)
            case "mark":
                if argument is not None:
                    argument = argument.rsplit(maxsplit=1)
                    if len(argument) == 1:
                        argument.append(None)
                    try:
                        argument[0] = int(argument[0])
                        Commands.mark(index=argument[0], value=argument[1])
                    except ValueError:
                        Commands.mark(name=argument[0], value=argument[1])
            case "remove" | "delete" | "rm" | "del" | "-":
                try:
                    argument = int(argument)
                    Commands.remove(index=argument)
                except ValueError:
                    Commands.remove(name=argument)
            case "save" | ":w":
                Commands.save(location=argument)
            case "view":
                if argument is not None:
                    try:
                        argument = int(argument)
                    except ValueError:
                        print("Commands::Parse(View): Error (argument non-int)")
                        return None
                Commands.view(index=argument)
            case "add" | "+":
                # suppress type mismatch complaints
                argument: typing.Any
                if argument is None:
                    argument = [None, None]
                else:
                    argument = argument.rsplit(maxsplit=1)
                    if len(argument) == 1:
                        argument.append(None)
                if argument[1] in Config["status"]["complete"]:
                    argument[1] = True
                else:
                    argument[1] = False
                Commands.add(task=argument)
            case _:
                print("Invalid command")

    @staticmethod
    def _str_task(task: list[str, bool], index: int = -1) -> str:
        task_format = Config["task_format"]
        if task[1]:
            status_box = Config["status_box"]["complete"]
        else:
            status_box = Config["status_box"]["incomplete"]
        if Commands._check_task(task):
            return task_format.format(index=index, status_box=status_box, name=task[0])
        else:
            print("Commands::_str_task(): Error (task failed check)")
            return ""

    @staticmethod
    def _check_task(task: list[str, bool]) -> bool:
        if isinstance(task[0], str) and isinstance(task[1], bool):
            return True
        else:
            return False

    @staticmethod
    def load(location: str = None) -> None:
        if location is None:
            location = Config["save_file"]
        global data
        file = open(location)
        data = json.loads(file.read())
        file.close()

    @staticmethod
    def save(location: str = None) -> None:
        if location is None:
            location = Config["save_file"]
        global data
        file = open(location, "w")
        file.write(json.dumps(data))
        file.close()

    @staticmethod
    def view(index: int = None) -> None:
        if index is None:
            index = None
        global data
        if index is None:
            i = 0
            for ITEM in data["tasks"]:
                print(Commands._str_task(ITEM, i))
                i += 1
        else:
            try:
                index = int(index)
            except ValueError:
                print("Commands::View(): Error (INDEX not int)")
                return None
            try:
                print(Commands._str_task(data["tasks"][index], index))
            except IndexError:
                print("Commands::View(): Error (INDEX out of range)")

    # TASK format: [name: str, completion: bool]
    @staticmethod
    def add(task: list[str, bool] = None) -> None:
        global data
        if task is not None:
            if Commands._check_task(task):
                data["tasks"].append(task)
            else:
                print("Commands::Add(): Error (TASK formatted improperly)")
        else:
            print("Commands::Add(): Error (no TASK)")
            return None

    @staticmethod
    def remove(index: int = None, name: str = None) -> None:
        global data
        if index is not None:
            try:
                index = int(index)
            except ValueError:
                print("Commands::Remove(): Error (INDEX not int)")
                return None
            try:
                data["tasks"].pop(index)
            except IndexError:
                print("Commands::Remove(): Error (INDEX out of range)")

        elif name is not None:
            print("Commands::Remove(): Error (removal on NAME not implemented")
        else:
            print("Commands::Remove(): Error (no INDEX or NAME)")

    @staticmethod
    def mark(index: int = None, name: str = None, value: typing.Any = None) -> None:
        global data
        if index is not None:
            try:
                index = int(index)
            except ValueError:
                print("Commands::Mark(): Error (INDEX not int)")
                return None
            try:
                if value in Config["status"]["complete"]:
                    data["tasks"][index][1] = True
                elif value in Config["status"]["incomplete"]:
                    data["tasks"][index][1] = False
                elif value is None:
                    data["tasks"][index][1] = bool((data["tasks"][index][1] + 1) % 2)
                else:
                    print("Commands::Mark(): Error (VALUE not complete/incomplete)")
            except IndexError:
                print("Commands::Mark(): Error (INDEX out of range)")

        elif name is not None:
            print("Commands::Remove(): Error (removal on NAME not implemented)")
        else:
            print("Commands::Remove(): Error (no INDEX or NAME)")

    @staticmethod
    def gpl(section: str = None) -> None:
        if section is not None:
            match section.lower():
                case "w" | "warranty" | "warrant" | "as is" | "warranties":
                    print(
                        """
  15. Disclaimer of Warranty.

  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
                    """
                    )
                case (
                    "c" | "copying" | "r" | "redistribute" | "redist" | "redistribution"
                ):
                    print(
                        """
  This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License Version 3 as published
by the Free Software Foundation.
                    """
                    )
                case _:
                    print("Commands::Gpl(): Error (SECTION not matched)")
                    return None
        else:
            print("Commands::Gpl(): Error (no SECTION)")
            return None

    @staticmethod
    def exit(save: bool = None, location: str = None) -> None:
        if save is None:
            save = True
        if location is None:
            location = Config["save_file"]
        if save:
            Commands.save(location=location)
        exit()

    @staticmethod
    def help(command: str = None) -> None:
        match command:
            case None:
                print(
                    """\
load LOCATION - loads file from LOCATION
save [LOCATION] - saves file [at LOCATION]
view [INDEX] - views all tasks [or task at index]
add TASK - adds TASK to task list
remove INDEX - removes task located at INDEX
mark INDEX [VALUE] - toggles completion state for task located at INDEX
-> mark INDEX VALUE - sets completion state for task at INDEX to VALUE
gpl SECTION - displays GPLv3 license SECTION
exit [LOCATION] - saves [at LOCATION] and exits
exit! - exits without saving
help [COMMAND] - displays help [for COMMAND]
"""
                )
            case _:
                print("Commands::Help(): Error (invalid COMMAND)")


# alloc for data, create fallback
data: dict = {"tasks": []}

print(
    """\
toad  Copyright (C) 2024  switz
This program comes with ABSOLUTELY NO WARRANTY; for details type `gpl w'.
This is free software, and you are welcome to redistribute it
under certain conditions; type `gpl c' for details.
"""
)

if os.path.exists(Config["save_file"]):
    if input("Save file detected, load? [Y/n] ").lower() in [
        "",
        "k",
        "ok",
        "y",
        "ye",
        "yes",
    ]:
        Commands.load(location=Config["save_file"])
        print("Loaded file")

while True:
    Commands.parse(input("λ> "))
